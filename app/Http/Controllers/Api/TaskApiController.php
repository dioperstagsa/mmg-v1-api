<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreTaskRequest;
use App\Http\Requests\Api\UpdateTaskRequest;
use App\Http\Resources\TaskResource;
use App\Http\Resources\TaskCollection;
use App\Models\Task;
use App\Models\Plant;
use App\Models\Zone;
use App\Models\TaskDefinition;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class TaskApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/task",
     *      operationId="listTask",
     *      tags={"Task"},
     *      summary="List all tasks",
     *      description="Returns tasks data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        return new TaskCollection(Task::all());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/task",
     *      operationId="storeTask",
     *      tags={"Task"},
     *      summary="Store new task",
     *      description="Returns task data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"date_beg, task_definition"},
     *              @OA\Property(property="name", type="string", example="Sowing Patatoes"),
     *              @OA\Property(property="date_beg", type="date", example="2023-08-12"),
     *              @OA\Property(property="date_end", type="date", example="2023-08-12"),
     *              @OA\Property(property="task_definition", type="integer", example="1"), 
     *              @OA\Property(property="plant", type="integer", example="1"),
     *              @OA\Property(property="zone", type="integer", example="1"),
     *              @OA\Property(property="description", type="string", example="..."),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreTaskRequest $request)
    {
        $task = Task::create($request->all());

        // Set plant
        $plant = Plant::find($request->plant);
        if(!empty($plant)){
            $task->plant_id = $plant->id;
        }

        // Set task definition
        $task_definition = TaskDefinition::find($request->task_definition);
        if(!empty($task_definition)){
            $task->task_definition_id = $task_definition->id;
        }
        // Set zone
        $zone = Zone::find($request->zone);
        if(!empty($zone)){
            $task->zone_id = $zone->id;
        }
        return (new TaskResource($task))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/task/{id}",
     *      operationId="getTaskById",
     *      tags={"Task"},
     *      summary="Get task information",
     *      description="Returns task data",
     *      @OA\Parameter(
     *          name="id",
     *          description="task id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={
     *                      "id": "1", 
     *                      "name": "Sowing potatoes",
     *                      "category": "Sow",
     *                      "task_definition": "{'id':'1', 'name':'Sowing potatoes'}",
     *                      "plant" : "{'id': '1', 'name':'Potato', 'plant_definition_id': '1'}",
     *                      "zone" : "{'id':'1', 'name':'Zone 1'}",
     *                      "description": "...",
     *                      "date_beg": "2020-12-08",
     *                      "date_end": "2020-12-08",
     *                  }
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show task  $id");
        $task = Task::findOrFail($id);
        return (new TaskResource($task))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/task/{id}",
     *      operationId="updateTask",
     *      tags={"Task"},
     *      summary="Update existing task",
     *      description="Returns updated task data",
     *      @OA\Parameter(
     *          name="id",
     *          description="task id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string", example="Sowing Patatoes"),
     *              @OA\Property(property="date_beg", type="date", example="2023-08-12"),
     *              @OA\Property(property="date_end", type="date", example="2023-08-12"),
     *              @OA\Property(property="task_definition", type="integer", example="1"), 
     *              @OA\Property(property="plant", type="integer", example="1"),
     *              @OA\Property(property="zone", type="integer", example="1"),
     *              @OA\Property(property="description", type="string", example="..."),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdateTaskRequest $request)
    {
        Log::debug("Udpate task $id");
        $task = Task::findOrFail($id);
        $task->update($request->all());

        return (new TaskResource($task))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/task/{id}",
     *      operationId="deleteTask",
     *      tags={"Task"},
     *      summary="Delete existing task",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="task id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(String $id)
    {
        $task = Task::find($id);
        $http_code =  Response::HTTP_NOT_FOUND;
        if(!empty($task)) {
            $task->delete();
            $http_code =  Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}