<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\PlantDefinitionRequest;

class UpdatePlantDefinitionRequest extends PlantDefinitionRequest
{
    use ApiRequestTrait;

}
