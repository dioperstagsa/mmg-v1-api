<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\TaskDefinitionRequest;

class UpdateTaskDefinitionRequest extends TaskDefinitionRequest
{
    use ApiRequestTrait;
}
